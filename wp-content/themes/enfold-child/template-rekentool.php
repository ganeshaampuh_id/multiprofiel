<?php
/*
  Template Name: Rekentool
 */
global $avia_config;

/*
 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
 */
get_header();


if (get_post_meta(get_the_ID(), 'header', true) != 'no')
	echo avia_title();

do_action('ava_after_main_title');
?>
<?php load_rekentool_scripts();do_shortcode('[formidable id=14]'); ?>
<div class='container_wrap container_wrap_first main_color <?php avia_layout_class('main'); ?>'>

	<div class='container'>

		<main class='template-page content  <?php avia_layout_class('content'); ?> units' <?php avia_markup_helper(array('context' => 'content', 'post_type' => 'page')); ?>>
			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */

			$avia_config['size'] = avia_layout_class('main', false) == 'entry_without_sidebar' ? '' : 'entry_with_sidebar';
			get_template_part('includes/loop', 'page');
			?>

			<div id="rekentool-wrapper">
				<p class="form-head" id="rekentool-headtitle"><strong>Bereken in 3 stappen de kosten van uw nieuwe entresolvloer</strong></p>
				<div id="tabcontainer">
					<form id="rekenform" method="POST" action="<?php echo site_url( 'rekentool-offerte' ); ?>">
						<div class="tab" id="tab1">
							<p class="form-sub">
								Met een entresolvloer van Multi Profiel haalt u het maximale rendement uit uw bedrijfsruimte. Geen verbouwing of nieuwbouw nodig! U verdubbelt eenvoudig uw beschikbare vierkante meters door het plaatsen van een entresolvloer.
							</p>
							<p class="form-sub">
								U weet nu wat het u op kan leveren, maar wat gaat een entresolvloer u kosten?<br />
								Doorloop de 3 stappen van onze rekentool en ontvang <strong><u>direct</u></strong> de automatisch berekende prijs in uw mailbox.
							</p>
							<div class="breadcrumb-wrapper">
								<ul id="breadcrumb">
									<li class="active">1</li>
									<li>2</li>
									<li>3</li>
									<li class="finish">UW<br />PRIJS</li>
								</ul>
							</div>
							<div class="tab-content">
								<h3>Afmetingen</h3>

								<h4>Geef aan wat de gewenste afmetingen zijn en het benodigde draagvermogen van uw entresolvloer.</h4>

								<div class="form-entry">
									<div class="form-label">Breedte (m)</div>
									<div class="form-field"><input data-bind="value: s1breedte" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="breedte" value="m" /></div>
									<div class="clearfix"></div>

									<div class="form-label">Diepte (m)</div>
									<div class="form-field"><input data-bind="value: s1diepte" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="lengte" value="m" /></div>
									<div class="clearfix"></div>

									<div class="form-label">Vrije hoogte onderzijde (m)</div>
									<div class="form-field with-tooltip"><input data-bind="value: s1hoogte" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="hoogte" value="m" /> <a href="#" title="Dit is de vrije hoogte, van de grond tot de onderkant van de entresolvloer. Andere klanten kiezen hier gemiddeld voor 3m." class="tooltip"><img src="<?php echo site_url(); ?>/rekentool-assets/tooltip.png" alt="" /></a></div>
									<div class="clearfix"></div>

									<div class="form-label">Draagvermogen (kg)</div>
									<div class="form-field with-tooltip"><input data-bind="value: s1draagvermogen" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="draagvermogen" value="m" /> <a href="#" title="Wat moet de entresolvloer maximaal kunnen dragen per m2? Andere klanten kiezen hier gemiddeld voor 500kg." class="tooltip"><img src="<?php echo site_url(); ?>/rekentool-assets/tooltip.png" alt="" /></a></div>
									<div class="clearfix"></div>
								</div>

								<div class="tab-picture">
									<img src="<?php echo site_url(); ?>/rekentool-assets/step-1.png" alt="Step 1 Image" />
								</div>
							</div>

							<div class="button-wrapper">
								<a class="formnav next" data-tab="tab2" data-validate="1" href="#stap2">Ga verder</a>
								<div class="clearfix"></div>
							</div>
							
							<p class="form-sub small" style="padding-top: 20px;">
								NB. Onze rekentool kan t/m een draagvermogen van 500 kg/m2 berekenen en houdt rekening met<br />
								veiligheidsklasse CC1. Meer informatie nodig? Mail naar <a href="mailto:info@multiprofiel.nl">info@multiprofiel.nl</a>
							</p>
						</div>
						<!-- end #tab1 -->
						<div class="tab" id="tab2">
							<p class="form-sub">
								Om de veiligheid van uw entresolvloer te waarborgen dient u de open zijdes van de entresolvloer te voorzien van een leuningconstructie.
							</p>
							<div class="breadcrumb-wrapper">
								<ul id="breadcrumb">
									<li><a href="#" class="formnav" data-tab="tab1">1</a></li>
									<li class="active">2</li>
									<li>3</li>
									<li class="finish">UW<br />PRIJS</li>
								</ul>
							</div>
							<div class="tab-content">
								<h3>Extra’s</h3>

								<h4>Geef aan wat de extra wensen zijn voor uw entresolvloer.</h4>

								<div class="form-entry">
									<div class="form-label radio">Een trap</div>
									<div class="form-field with-tooltip">
										<div class="radio">
											<input id="jatrap" type="radio" name="s2trap" value="true" data-bind="checked:s2trap.doCheck" />
											<label for="jatrap">ja</label>
										</div>
										<div class="radio">
											<input id="neetrap" type="radio" name="s2trap" value="false" data-bind="checked:s2trap.doCheck" />
											<label for="neetrap">nee</label>
										</div>
										<a href="#" title="Sluit u deze entresolvloer aan op een bestaande verdieping waar u reeds een trap heeft, dan is een trap misschien niet nodig." class="tooltip"><img src="<?php echo site_url(); ?>/rekentool-assets/tooltip.png" alt="" /></a>
									</div>
									<div class="clearfix"></div>

									<div class="form-label radio">Een leuningconstructie</div>
									<div class="form-field with-tooltip">
										<div class="radio">
											<input id="ja" type="radio" name="s2leuning" value="true" data-bind="checked:s2leuning.doCheck" />
											<label for="ja">ja</label>
										</div>
										<div class="radio">
											<input id="nee" type="radio" name="s2leuning" value="false" data-bind="checked:s2leuning.doCheck" />
											<label for="nee">nee</label>
										</div>
										<a href="#" title="Om de veiligheid van uw entresolvloer te waarborgen, dient u de open zijden van de entresolvloer te voorzien van een leuningconstructie." class="tooltip"><img src="<?php echo site_url(); ?>/rekentool-assets/tooltip.png" alt="" /></a>
									</div>
									<div class="clearfix"></div>

									<div id="aantal-meters">
										<div class="form-label large">
											<img src="<?php echo site_url(); ?>/rekentool-assets/field-label-large.png" alt="" class="field-label-large" /> Aantal meters leuningconstructie
										</div>
										<div class="form-field small with-tooltip">
											<input data-bind="value: s2leuninglengte" class="numberinput single validate[custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" step="0.01" name="s2leuninglengte" value="0" /> <a href="#" title="Graag het totaal aantal strekkende meters invoeren." class="tooltip"><img src="<?php echo site_url(); ?>/rekentool-assets/tooltip.png" alt="" /></a>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>

								<div class="tab-picture">
									<img src="<?php echo site_url(); ?>/rekentool-assets/step-2.png" alt="Step 2 Image" />
								</div>
							</div>
							<div class="button-wrapper">
								<a class="formnav nexttwo" data-tab="tab1" href="#stap1">Ga Terug</a>  
								<a class="formnav next" data-tab="tab3" data-validate="2" href="#stap3">Ga verder</a>
								<div class="clearfix"></div>
							</div>
						</div>
						<!-- end #tab2 -->
						<div class="tab" id="tab3">
							<p class="form-sub">
								Alle entresolvloeren worden door Multi Profiel op locatie ingemeten, waarna een werktekening wordt gemaakt welke ter controle aan u wordt gezonden. Multi Profiel blijft uiteraard verantwoordelijk voor de maatvoering van de entresolvloer.
							</p>
							<p class="form-sub">
								Multi Profiel heeft daarnaast meerdere montageteams die gespecialiseerd zijn in het monteren van entresolvloeren. Wilt u de entresolvloer laten monteren door één van onze montageteams?
							</p>

							<div class="breadcrumb-wrapper">
								<ul id="breadcrumb">
									<li><a href="#" class="formnav" data-tab="tab1">1</a></li>
									<li><a href="#" class="formnav" data-tab="tab2">2</a></li>
									<li class="active">3</li>
									<li class="finish">UW<br />PRIJS</li>
								</ul>
							</div>
							<div class="tab-content">
								<h3>Montage</h3>

								<h4>Geef aan of u de entresolvloer wilt laten monteren door Multi Profiel.</h4>

								<div class="form-entry">
									<div class="form-label">Montage meeberekenen?</div>
									<div class="form-field with-tooltip">
										<div class="radio">
											<input data-bind="checked:s3montage.doCheck" type="radio" name="s3montage" id="ja" value="true" />
											<label for="ja">ja</label>
										</div>
										<div class="radio">
											<input data-bind="checked:s3montage.doCheck" type="radio" name="s3montage" id="nee" value="false" />
											<label for="nee">nee</label>
										</div>
										<a href="#" title="Multi Profiel heeft meerdere montageteams die gespecialiseerd zijn in het monteren van entresolvloeren." class="tooltip"><img src="<?php echo site_url(); ?>/rekentool-assets/tooltip.png"  /></a>
									</div>
									<div class="clearfix"></div>
								</div>

								<div class="tab-picture">
									<img src="<?php echo site_url(); ?>/rekentool-assets/step-3.png" alt="Step 3 Image" />
								</div>
							</div>
							<div class="button-wrapper">
								<a class="formnav nexttwo" data-tab="tab2" href="#stap2">Ga Terug</a>
								<a class="formnav next" data-tab="tab4" data-validate="3" href="#stap4">Ga verder</a>
								<div class="clearfix"></div>
							</div>
						</div>
						<!-- end #tab3 -->
						<div class="tab" id="tab4">
							<div class="breadcrumb-wrapper">
								<ul id="breadcrumb">
									<li><a href="#" class="formnav" data-tab="tab1">1</a></li>
									<li><a href="#" class="formnav" data-tab="tab2">2</a></li>
									<li><a href="#" class="formnav" data-tab="tab3">3</a></li>
									<li class="finish active">UW<br />PRIJS</li>
								</ul>
							</div>

							<div class="tab-content">
								<h3>Uw gegevens </h3>
								<h4>Vul hieronder uw gegevens in en ontvang <strong><u>direct</u></strong> de berekening in uw mailbox.</h4>

								<div class="form-entry">
									<div class="form-label">Bedrijfsnaam</div>
									<div class="form-field"><input type="text" name="naambedrijf"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">Naam contactpersoon</div>
									<div class="form-field"><input type="text" name="naam"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">E-mailadres</div>
									<div class="form-field"><input type="text" name="email"  class="validate[required,custom[email]] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">Telefoonnummer</div>
									<div class="form-field"><input type="text" name="telefoon"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">Adres</div>
									<div class="form-field"><input type="text" name="adres" class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">Postcode</div>
									<div class="form-field"><input type="text" name="postcode" class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">Plaats</div>
									<div class="form-field"><input type="text" name="plaats" class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>

									<div class="form-label">Land</div>
									<div class="form-field"><input type="text" name="land" class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></div>
									<div class="clearfix"></div>
								</div>

								<div class="tab-picture">
									<img src="<?php echo site_url(); ?>/rekentool-assets/step-4.png" alt="Step 4 Image" />
								</div>
								
								<input data-bind="value: prijsvloerkortingdisplay" type="hidden" name="prijsmateriaal" readonly  class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
								<input data-bind="value: prijsaccessoiresdisplay" readonly  type="hidden" name="prijsaccessoires"  class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
								<input data-bind="value: prijsmontagedisplay" type="hidden" name="prijsmontage" readonly class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
								<input data-bind="value: prijstotaal" readonly  type="hidden" name="prijstotaal" class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
							</div>		
							<!-- <li>Land </li><span class="resultinput"><input type="text" name="land"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
							<li>Plaats </li><span class="resultinput"><input type="text" name="plaats"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
							<li>Adres </li><span class="resultinput"><input type="text" name="adres"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
							<li>Postcode </li><span class="resultinput"><input type="text" name="postcode"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span> -->
							<div class="button-wrapper submit">
								<input type="submit" style="border:none; padding:9px; cursor:pointer" value="Stuur mijn prijs!" class="verderthree">
								<div class="clearfix"></div>
							</div>
						</div>
						<!-- end #tab4 -->
					</form>
				</div>
				<script>
					jQuery = jQuery.noConflict();
					jQuery(document).ready(function ($) {

						jQuery('input[type="text"]').click(function () {
							if (jQuery(this).val() == '0') {
								jQuery(this).val('');
							}
						});

						jQuery('#rekenform').validationEngine();

						jQuery('#tabcontainer .tab').hide();
						jQuery('#tabcontainer #tab1').show();

						//jQuery('#tab4 .formnav.nexttwo').click(function(){
						//	jQuery.ajax({
						//		type: "POST",
						//		url: "offerte.php?stap=4",
						//		data: jQuery("#rekenform").serialize()
						//	}).done(function() {
						//
						//	});
						//});

						jQuery('.formnav').click(function () {
							var tab = jQuery(this).data('tab');
							var valtab = jQuery(this).data('validate');

							//console.log(tab);
							//console.log(valtab);

							var isvalid = true;



							if (jQuery(this).hasClass('next') || jQuery(this).hasClass('nexttwo')) {
								jQuery('#tab' + valtab + ' input.required').each(function () {
									//console.log(jQuery(this));
									if (jQuery(this).val() == 0) {
										isvalid = false;
										jQuery(this).css('border', '1px solid #c00');
									} else {
										jQuery(this).css('border', '1px solid #bfbfbf');
									}
								})
								if (isvalid == true) {
									jQuery('#tabcontainer .tab').hide();
									jQuery('#tabcontainer #' + tab).show();
								}
							} else {
								jQuery('#tabcontainer .tab').hide();
								jQuery('#tabcontainer #' + tab).show();
							}

							return false;
						});

						// jQuery("input[type=number]").on("keydown", function(e) {
                        //     console.log(e.keyCode);
                        //     if(e.keyCode === 190 || e.keyCode === 110) {
                        //         this.value.replace('.', ',');
                        //         e.preventDefault();
                        //     }
                        // });

                        $('input[name="s2leuning"]').change(function(){
                        	$val = $(this).val();
                        	$aantalmeters = $('#aantal-meters');

                        	if($val == 'true'){
                        		$aantalmeters.slideDown();
                        	}else{
                        		$aantalmeters.slideUp();
                        	}
                        });
					});

					jQuery('#rekenform').keypress(function (event) {
						if (event.keyCode == 10 || event.keyCode == 13)
							event.preventDefault();
					});

                    jQuery.fn.ForceNumericOnly =
                    function()
                    {
                		return this.each(function()
                		{
                			jQuery(this).keydown(function(e)
                			{
                                if(e.keyCode == 188){
                                    e.preventDefault();
                                    jQuery(this).val(jQuery(this).val() + '.');
                                }
                				var key = e.charCode || e.keyCode || 0;
                				return (
                					key == 8 ||
                					key == 9 ||
                					key == 46 ||
                					key == 110 ||
                                    key == 188 ||
                					key == 190 ||
                					(key >= 35 && key <= 40) ||
                					(key >= 48 && key <= 57) ||
                					(key >= 96 && key <= 105));

                			});
                		});
                	};
                    jQuery(".numberinput").ForceNumericOnly();
				</script>

				<script src="<?php echo site_url(); ?>/rekentool-assets/knockout.js"></script>
				<script src="<?php echo site_url(); ?>/rekentool-assets/calc.js"></script>
				<!--<hr />

				<div class="images_top">
					<div class="images_top-row first"><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-02.jpg" rel="lightbox[roadtrip]"><img class="first" style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-2-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-03.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-3-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-04.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-4-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-05.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-5-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a></div>
					<div class="images_top-row first"><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-06.jpg" rel="lightbox[roadtrip]"><img class="first" style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-6-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-07.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-7-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-08.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-8-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-01.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-9-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a></div>
					</div>-->
			</div><!-- .entry-content -->
		</main>

		<?php
		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();
		?>

	</div><!--end container-->

</div><!-- close default .container_wrap element -->

<!-- Start Of Script Generated By cforms v14.6 [Oliver Seidel | www.deliciousdays.com] -->
<!-- End Of Script Generated By cforms -->

<!-- Google Code for Lijst 15 mei 2013 -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1070683703;
var google_conversion_label = "9LtXCJmmvgUQt6zF_gM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="<?php echo site_url(); ?>/rekentool-assets/conversion.js">
</script>
<?php get_footer(); ?>
