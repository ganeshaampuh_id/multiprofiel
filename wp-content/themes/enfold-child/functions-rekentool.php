<?php
function load_rekentool_scripts() {
	echo '<link rel="stylesheet" type="text/css" href="' . site_url() . '/rekentool-assets/cforms2012.css">';
	echo '<link rel="stylesheet" type="text/css" href="' . site_url() . '/rekentool-assets/custom.css">';
	echo '<link rel="stylesheet" type="text/css" href="' . site_url() . '/rekentool-assets/css/validationEngine.jquery.css">';

	echo '<script type="text/javascript" async src="' . site_url() . '/rekentool-assets/dc.js" async></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/prototype.js"></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/wp-scriptaculous.js" async></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/effects.js" async></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/lightbox.js" async></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/jquery.js"></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/jcycle.js" async></script>';
	echo '<script src="' . site_url() . '/rekentool-assets/js/jquery.validationEngine.js" async></script>';
	echo '<script src="' . site_url() . '/rekentool-assets/js/languages/jquery.validationEngine-nl.js" async></script>';
	echo '<script type="text/javascript" src="' . site_url() . '/rekentool-assets/cforms.js" async></script>';
}

function sendMail() {
	$naam = (string) $_POST['naam'];
	$naambedrijf = (string) $_POST['naambedrijf'];
	$land = (string) $_POST['land'];
	$plaats = (string) $_POST['plaats'];
	$postcode = (string) $_POST['postcode'];
	$adres = (string) $_POST['adres'];
	$telefoon = (string) $_POST['telefoon'];
	$email = (string) $_POST['email'];

	$klantemail = (string) $_POST['s4mail'];

	$breedte = (string) $_POST['breedte'];
	$diepte = (string) $_POST['lengte'];
	$hoogte = (string) $_POST['hoogte'];
	$draagvermogen = (string) $_POST['draagvermogen'];

	$trap = (string) $_POST['s2trap'];
	$vloerplaat = (string) $_POST['s2vloerplaat'];
	$leuning = (string) $_POST['s2leuning'];
	$leuninglengte = (string) $_POST['s2leuninglengte'];

	$montage = (string) $_POST['s3montage'];

	$prijsmateriaal = (string) $_POST['prijsmateriaal'];
	$prijsaccessoires = (string) $_POST['prijsaccessoires']; 
	$prijsmontage = (string) $_POST['prijsmontage'];
	$prijstotaal = (string) $_POST['prijstotaal'];
	if ($prijstotaal = '0,00') {
		$prijstotaal = floatval(str_replace(',','.',str_replace('.','',$prijsmateriaal))) + floatval(str_replace(',','.',str_replace('.','',$prijsaccessoires))) + floatval(str_replace(',','.',str_replace('.','',$prijsmontage)));
		$prijstotaal = number_format($prijstotaal, 2, ',', '.');
	}

	$from_email_address = get_field( 'from_email_address', 'options' );
	$from_name = get_field( 'from_name', 'options' );
	$subject = get_field( 'subject', 'options' );
	$to_email_address = get_field( 'to_email_address', 'options' );

	$headers[] = "From: $from_name <$from_email_address>";
	$headers[] = "Bcc: $email";
	$headers[] = "Content-Type: text/html; charset=iso-8859-1";
	
	$body_open = '<html>
		<head><title>Hartelijk dank voor uw aanvraag - Multi Profiel Rekentool</title>
		<style type="text/css">
		body {
			font-family:arial;
			font-size:12px;
			color: #333;
		}
		p {
			font-family:arial;
			font-size:12px;
			color: #333;
			margin: 0 0 2em 0;
			padding: 0;
		}
		</style>
		</head>
		<body>
	';

	$content = get_field( 'email_format', 'options' );
	$content = str_replace( "[naam]", $naam, $content );
	$content = str_replace( "[naambedrijf]", $naambedrijf, $content );
	$content = str_replace( "[telefoon]", $telefoon, $content );
	$content = str_replace( "[email]", $email, $content );
	$content = str_replace( "[adres]", $adres, $content );
	$content = str_replace( "[postcode]", $postcode, $content );
	$content = str_replace( "[plaats]", $plaats, $content );
	$content = str_replace( "[land]", $land, $content );
	$content = str_replace( "[breedte]", $breedte, $content );
	$content = str_replace( "[diepte]", $diepte, $content );
	$content = str_replace( "[hoogte]", $hoogte, $content );
	$content = str_replace( "[draagvermogen]", $draagvermogen, $content );
	$content = str_replace( "[trap]", $trap, $content );
	$content = str_replace( "[leuning]", $leuning, $content );
	$content = str_replace( "[leuninglengte]", $leuninglengte, $content );
	$content = str_replace( "[montage]", $montage, $content );
	$content = str_replace( "[prijsmateriaal]", $prijsmateriaal, $content );
	$content = str_replace( "[prijsaccessoires]", $prijsaccessoires, $content );
	$content = str_replace( "[prijsmontage]", $prijsmontage, $content );
	$content = str_replace( "[prijstotaal]", $prijstotaal, $content );

	$body_close = '</body></html>';
	$html = $body_open . $content . $body_close;

	if ( wp_mail( $to_email_address, $subject, $html, $headers ) ) {
		$fields = array();
		$fields[] = $naam;
		$fields[] = $naambedrijf;
		$fields[] = $telefoon;
		$fields[] = $email;
		$fields[] = $adres;
		$fields[] = $postcode;
		$fields[] = $plaats;
		$fields[] = $land;
		$fields[] = $breedte;
		$fields[] = $diepte;
		$fields[] = $hoogte;
		$fields[] = $draagvermogen;
		$fields[] = $trap;
		$fields[] = $vloerplaat;
		$fields[] = $leuning;
		$fields[] = $leuninglengte;
		$fields[] = $montage;
		$fields[] = $prijsmateriaal;
		$fields[] = $prijsaccessoires;
		$fields[] = $prijsmontage;
		$fields[] = $prijstotaal;
		$fields[] = date('d-m-Y h:i:s A');
		$fp = fopen('csv/rekentool.csv', 'a');
		fputcsv($fp, $fields, ';');
		fclose($fp);
		
		echo 'Hartelijk dank voor uw aanvraag. We hebben een kopie van uw ingevoerde data naar het door u opgegeven emailadres verzonden.';
	}
}

function sendMail2() {
	$klantemail = (string) $_POST['s4mail'];

	$breedte = (string) $_POST['breedte'];
	$diepte = (string) $_POST['lengte'];
	$hoogte = (string) $_POST['hoogte'];
	$draagvermogen = (string) $_POST['draagvermogen'];

	$trap = (string) $_POST['s2trap'];
	$vloerplaat = (string) $_POST['s2vloerplaat'];
	$leuning = (string) $_POST['s2leuning'];
	$leuninglengte = (string) $_POST['s2leuninglengte'];

	$montage = (string) $_POST['s3montage'];

	$prijsmateriaal = (string) $_POST['prijsmateriaal'];
	$prijsaccessoires = (string) $_POST['prijsaccessoires'];
	$prijsmontage = (string) $_POST['prijsmontage'];
	$prijstotaal = (string) $_POST['prijstotaal'];

	$from_email_address_2 = get_field( 'from_email_address_2', 'options' );
	$from_name_2 = get_field( 'from_name_2', 'options' );
	$subject_2 = get_field( 'subject_2', 'options' );
	$to_email_address_2 = get_field( 'to_email_address_2', 'options' ); 

	$to[] = $klantemail; 
	$to[] = $to_email_address_2;

	$headers[] = "From: $from_name_2 <$from_email_address_2>";
	$headers[] = "Cc: $from_name_2 <$from_email_address_2>";
	$headers[] = "MIME-Version: 1.0";
	$headers[] = "Content-type: text/html; charset=iso-8859-1";

	$body_open = '<html>
			<head><title>Hartelijk dank voor uw aanvraag - Multi Profiel Rekentool</title>
			<style type="text/css">
			body {
				font-family:arial;
				font-size:12px;
				color: #333;
			}
			p {
				font-family:arial;
				font-size:12px;
				color: #333;
				margin: 0 0 2em 0;
				padding: 0;
			}
			</style>
			</head>
			<body>';

	$content = get_field( 'email_format_2', 'options' );
	$content = str_replace( "[breedte]", $breedte, $content );
	$content = str_replace( "[diepte]", $diepte, $content );
	$content = str_replace( "[hoogte]", $hoogte, $content );
	$content = str_replace( "[draagvermogen]", $draagvermogen, $content );
	$content = str_replace( "[trap]", $trap, $content );
	$content = str_replace( "[vloerplaat]", $vloerplaat, $content );
	$content = str_replace( "[leuning]", $leuning, $content );
	$content = str_replace( "[leuninglengte]", $leuninglengte, $content );
	$content = str_replace( "[montage]", $montage, $content );
	$content = str_replace( "[prijsmateriaal]", $prijsmateriaal, $content );
	$content = str_replace( "[prijsaccessoires]", $prijsaccessoires, $content );
	$content = str_replace( "[prijsmontage]", $prijsmontage, $content );
	$content = str_replace( "[prijstotaal]", $prijstotaal, $content );

	$body_close = '</body></html>';
	$html = $body_open . $content . $body_close;

	if ( wp_mail( $to, $subject_2, $html, $headers ) ) {
		$fields = array();
		$fields[] = '';
		$fields[] = '';
		$fields[] = '';
		$fields[] = '';
		$fields[] = '';
		$fields[] = '';
		$fields[] = '';
		$fields[] = '';
		$fields[] = $breedte;
		$fields[] = $diepte;
		$fields[] = $hoogte;
		$fields[] = $draagvermogen;
		$fields[] = $trap;
		$fields[] = $vloerplaat;
		$fields[] = $leuning;
		$fields[] = $leuninglengte;
		$fields[] = $montage;
		$fields[] = $prijsmateriaal;
		$fields[] = $prijsaccessoires;
		$fields[] = $prijsmontage;
		$fields[] = $prijstotaal;
		$fields[] = date('d-m-Y h:i:s A');
		$fp = fopen('csv/rekentool.csv', 'a');
		fputcsv($fp, $fields, ';');
		fclose($fp);
		
		echo 'Hartelijk dank voor uw aanvraag. We hebben een kopie van uw ingevoerde data naar het door u opgegeven emailadres verzonden.';
	}
}

function validateForm() {
	if ( $_GET['stap'] == 4 ) {
		sendMail2();
	} else {
		if ( !empty( $_POST ) ) {
			sendMail();
		} else {
			echo '<p>Hartelijk dank voor uw aanvraag! Wij gaan er direct mee aan de slag en u kunt binnen 24 uur een reactie van ons ontvangen. We hebben een kopie van uw ingevoerde data naar het door u opgegeven emailadres verzonden, zodat u dit altijd nog even na kunt kijken.</p>';
		}
	}
}