<?php
add_action( 'after_setup_theme', 'enfold_woocommerce_child_config' );
function enfold_woocommerce_child_config() {
    remove_action( 'woocommerce_before_shop_loop', 'avia_woocommerce_before_shop_loop', 1);
    add_action( 'woocommerce_before_shop_loop', 'avia_woocommerce_before_shop_loop_child', 1);
    
    remove_action( 'woocommerce_after_shop_loop', 'avia_woocommerce_after_shop_loop', 10);
    add_action( 'woocommerce_after_shop_loop', 'avia_woocommerce_after_shop_loop_child', 10);
    
    remove_action( 'woocommerce_before_shop_loop', 'avia_woocommerce_overview_banner_image', 10);
    add_action( 'woocommerce_before_shop_loop', 'avia_woocommerce_overview_banner_image_child', 10);

    remove_action( 'woocommerce_before_shop_loop', 'avia_woocommerce_frontend_search_params', 20);
    add_action( 'woocommerce_before_shop_loop', 'avia_woocommerce_frontend_search_params_child', 20);

    function avia_woocommerce_before_shop_loop_child() {
        global $avia_config;
        if(isset($avia_config['dynamic_template'])) return;
        $markup = avia_markup_helper(array('context' => 'content','echo'=>false,'post_type'=>'products'));
        echo "<main class='template-shop content ".avia_layout_class( 'content' , false)." units' $markup><div class='entry-content-wrapper'>";

        echo "<div class='webwinkel-leftsidebar'>";
        dynamic_sidebar( 'webwinkel-left' );
        echo "</div>";
        echo "<div class='webwinkel-grids'>";    
    }
    
    function avia_woocommerce_after_shop_loop_child() {
        global $avia_config;
        
        if(isset($avia_config['dynamic_template'])) return;
        if(isset($avia_config['overview'] )) echo avia_pagination('', 'nav');
        
        echo "</div>";
        echo "<div class='webwinkel-rightsidebar'>";
        dynamic_sidebar( 'webwinkel-right' );
        echo "</div>";
        echo "<div style='clear:both;'>";
        echo "</div>";
        echo "</div></div></main>";
    }
    
    function avia_woocommerce_overview_banner_image_child()
    {
        global $avia_config;
        if(avia_is_dynamic_template() || is_paged() || is_search() ) return false;

        $image_size = "entry_with_sidebar";
        $layout = avia_layout_class( 'main' , false );
        if($layout == 'fullsize') $image_size = 'entry_without_sidebar';

        if(is_shop())
        {
            $shop_id = woocommerce_get_page_id('shop');
            if($shop_id != -1)
            {
                $image = get_the_post_thumbnail($shop_id, $image_size);
                if($image) echo "<div class='page-thumb'>{$image}</div>";
            }
        }

        if(is_product_category())
        {
            global $wp_query;
            $image	= "";
        }
    }

    function avia_woocommerce_frontend_search_params_child()
	{
		global $avia_config;

		if (get_queried_object()->count > 0) {
			if(!empty($avia_config['woocommerce']['disable_sorting_options'])) return false;

			$product_order['default'] 	= __("Default Order",'avia_framework');
			$product_order['title'] 	= __("Name",'avia_framework');
			$product_order['price'] 	= __("Price",'avia_framework');
			$product_order['date'] 		= __("Date",'avia_framework');
			$product_order['popularity'] = __("Popularity",'avia_framework');

			$product_sort['asc'] 		= __("Click to order products ascending",  'avia_framework');
			$product_sort['desc'] 		= __("Click to order products descending",  'avia_framework');

			$per_page_string 		 	= __("Products per page",'avia_framework');


			$per_page 		 		 	= get_option('avia_woocommerce_product_count');
			if(!$per_page) $per_page 	= get_option('posts_per_page');
			if(!empty($avia_config['woocommerce']['default_posts_per_page'])) $per_page = $avia_config['woocommerce']['default_posts_per_page'];


			parse_str($_SERVER['QUERY_STRING'], $params);

			$po_key = !empty($avia_config['woocommerce']['product_order']) ? $avia_config['woocommerce']['product_order'] : 'default';
			$ps_key = !empty($avia_config['woocommerce']['product_sort'])  ? $avia_config['woocommerce']['product_sort'] : 'asc';
			$pc_key = !empty($avia_config['woocommerce']['product_count']) ? $avia_config['woocommerce']['product_count'] : $per_page;

			$ps_key = strtolower($ps_key);
			
			$nofollow = 'rel="nofollow"';

			//generate markup
			$output  = "";
			$output .= "<div class='product-sorting'>";
			$output .= "    <ul class='sort-param sort-param-order'>";
			$output .= "    	<li><span class='currently-selected'>".__("Sort by",'avia_framework')." <strong>".$product_order[$po_key]."</strong></span>";
			$output .= "    	<ul>";
			$output .= "    	<li".avia_woo_active_class($po_key, 'default')."><a href='".avia_woo_build_query_string($params, 'product_order', 'default')."' {$nofollow}>	<span class='avia-bullet'></span>".$product_order['default']."</a></li>";
			$output .= "    	<li".avia_woo_active_class($po_key, 'title')."><a href='".avia_woo_build_query_string($params, 'product_order', 'title')."' {$nofollow}>	<span class='avia-bullet'></span>".$product_order['title']."</a></li>";
			$output .= "    	<li".avia_woo_active_class($po_key, 'price')."><a href='".avia_woo_build_query_string($params, 'product_order', 'price')."' {$nofollow}>	<span class='avia-bullet'></span>".$product_order['price']."</a></li>";
			$output .= "    	<li".avia_woo_active_class($po_key, 'date')."><a href='".avia_woo_build_query_string($params, 'product_order', 'date')."' {$nofollow}>	<span class='avia-bullet'></span>".$product_order['date']."</a></li>";
			$output .= "    	<li".avia_woo_active_class($po_key, 'popularity')."><a href='".avia_woo_build_query_string($params, 'product_order', 'popularity')."' {$nofollow}>	<span class='avia-bullet'></span>".$product_order['popularity']."</a></li>";
			$output .= "    	</ul>";
			$output .= "    	</li>";
			$output .= "    </ul>";

			$output .= "    <ul class='sort-param sort-param-sort'>";
			$output .= "    	<li>";
			if($ps_key == 'desc') 	$output .= "    		<a title='".$product_sort['asc']."' class='sort-param-asc'  href='".avia_woo_build_query_string($params, 'product_sort', 'asc')."' {$nofollow}>".$product_sort['desc']."</a>";
			if($ps_key == 'asc') 	$output .= "    		<a title='".$product_sort['desc']."' class='sort-param-desc' href='".avia_woo_build_query_string($params, 'product_sort', 'desc')."' {$nofollow}>".$product_sort['asc']."</a>";
			$output .= "    	</li>";
			$output .= "    </ul>";

			$output .= "    <ul class='sort-param sort-param-count'>";
			$output .= "    	<li><span class='currently-selected'>".__("Display",'avia_framework')." <strong>".$pc_key." ".$per_page_string."</strong></span>";
			$output .= "    	<ul>";
			$output .= "    	<li".avia_woo_active_class($pc_key, $per_page).">  <a href='".avia_woo_build_query_string($params, 'product_count', $per_page)."' {$nofollow}>		<span class='avia-bullet'></span>".$per_page." ".$per_page_string."</a></li>";
			$output .= "    	<li".avia_woo_active_class($pc_key, $per_page*2)."><a href='".avia_woo_build_query_string($params, 'product_count', $per_page * 2)."' {$nofollow}>	<span class='avia-bullet'></span>".($per_page * 2)." ".$per_page_string."</a></li>";
			$output .= "    	<li".avia_woo_active_class($pc_key, $per_page*3)."><a href='".avia_woo_build_query_string($params, 'product_count', $per_page * 3)."' {$nofollow}>	<span class='avia-bullet'></span>".($per_page * 3)." ".$per_page_string."</a></li>";
			$output .= "    	</ul>";
			$output .= "    	</li>";
			$output .= "	</ul>";



			$output .= "</div>";
			echo $output;
		}
	}    
    
}

function webwinkel_sidebar_init() {

	register_sidebar( array(
		'name' => __( 'Webwinkel Left', 'webwinkel' ),
		'id' => 'webwinkel-left',
		'description' => __( 'Appears on the webwinkel left sidebar', 'webwinkel' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' =>__( 'Webwinkel Right', 'webwinkel'),
		'id' => 'webwinkel-right',
		'description' => __( 'Appears on the webwinkel right sidebar', 'webwinkel' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}

add_action( 'widgets_init', 'webwinkel_sidebar_init' );

add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
function enqueue_font_awesome() {
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );

}

// Add Shortcode
function show_cart($atts) {
	ob_start();
		global $woocommerce;
		$viewing_cart = __('View your shopping cart', 'your-theme-slug');
		$start_shopping = __('Start shopping', 'your-theme-slug');
		$cart_url = $woocommerce->cart->get_cart_url();
		$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
		$cart_contents_count = $woocommerce->cart->cart_contents_count;
		$cart_contents = sprintf(_n('%d item', '%d items', $cart_contents_count, 'your-theme-slug'), $cart_contents_count);
		$cart_total = $woocommerce->cart->get_cart_total();
		// Uncomment the line below to hide nav menu cart item when there are no items in the cart
		// if ( $cart_contents_count > 0 ) {
			if ($cart_contents_count == 0) {
				//$menu_item = '<li class="right"><a class="wcmenucart-contents" href="'. $shop_page_url .'" title="'. $start_shopping .'">';
                $menu_item = '<div class="avia-button-wrap avia-button-left avia-builder-el-32" style="width:100%;">';
                $menu_item .= '<a class="avia-button  avia-icon_select-yes-left-icon avia-color-theme-color avia-size-large avia-position-left" style="width:100%;text-align:left;">';
                $menu_item .= '<i class="fa fa-shopping-cart" style="font-size: 24px;"></i> ';
                $menu_item .= '<span class="avia_iconbox_title" style="font-size: 14px;margin-left:10px;">Leeg</span>';
                $menu_item .= '</a>';
                $menu_item .= '</div>';
			} else {
                $menu_item = '<div class="avia-button-wrap avia-button-left avia-builder-el-32" style="width:100%;">';
                $menu_item .= '<a href="'. $cart_url .'" class="avia-button  avia-icon_select-yes-left-icon avia-color-theme-color avia-size-large avia-position-left" style="width:100%;text-align:left;">';
                $menu_item .= '<i class="fa fa-shopping-cart" style="font-size: 24px;"></i> ';

                $menu_item .= '<span class="avia_iconbox_title" style="font-size: 14px;margin-left:10px;">'. $cart_contents.' - '. $cart_total .'</span>';
                $menu_item .= '</a>';
                $menu_item .= '</div>';
			}
		// Uncomment the line below to hide nav menu cart item when there are no items in the cart
		// }
		echo $menu_item;
	$social = ob_get_clean();
	return $social;

}
add_shortcode( 'showcart', 'show_cart' );

add_filter( 'woocommerce_subcategory_count_html', 'woo_remove_category_products_count' );

function woo_remove_category_products_count() {
  return;
}

add_filter( 'get_terms', 'get_subcategory_terms', 10, 3 );

function get_subcategory_terms( $terms, $taxonomies, $args ) { $new_terms = array(); 

    // if a product category and on the shop page/archive page/single product page
    if ( in_array( 'product_cat', $taxonomies ) && is_shop() || in_array( 'product_cat', $taxonomies ) && is_archive() || in_array( 'product_cat', $taxonomies ) && is_single() ) { 
        foreach ( $terms as $key => $term ) { 
            if ( !in_array( $term->slug, array(
                'opklapbare-wagens',
                'platvormwagens',
                'zwarte-tafelwagens',
                'lichte-tafelwagens',
                'eurobakken-rolplateau-s',
                'opzetramen',
                'magazijnbakken-eurobakken',
                'eurobakken-etageplateau-s',
                'meubelrolplateau-s',
                'breedte-1-050-mm-1',
                'verscholen',
                'breedte-1-200-mm',
                'magazijn-inrichting',
                'uncategorised'
                ) ) ) {
                $new_terms[] = $term;
            } 
        } 
        $terms = $new_terms;
    } 
    return $terms;
}

add_filter('wpseo_title', 'add_to_page_titles');
function add_to_page_titles($title) {
    if (is_woocommerce() || is_cart() || is_checkout()) {
        $title = explode(' - ', $title)[0];
        $title = str_replace('Archief ', '', $title);
        $title = str_replace(' Archief', '', $title);
        $title = str_replace('Archief', '', $title);
        $title = str_replace('Archieven ', '', $title);
        $title = str_replace(' Archieven', '', $title);
        $title = str_replace('Archieven', '', $title);
    }
    
    return $title;
}