<?php
/*
  Template Name: Webwinkelshop
 */
	if ( !defined('ABSPATH') ){ die(); }
	
	global $avia_config;

	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	 get_header();

//hjhbjnklkmmlmljbhvhccgfhjh
 	 if( get_post_meta(get_the_ID(), 'header', true) != 'no') echo avia_title();
 	 
 	 do_action( 'ava_after_main_title' );
	 ?>

		<div class='container_wrap container_wrap_first main_color <?php avia_layout_class( 'main' ); ?>'>
			<div class='container'>
				<main class='template-page content template-shop content av-content-small alpha units' <?php avia_markup_helper(array('context' => 'content','post_type'=>'page'));?>>
					<div class="flex_column av_one_fifth flex_column_div av-zero-column-padding first avia-builder-el-0 el_before_av_four_fifth avia-builder-el-first" style="border-radius:0px;">
					</div>
					<div class="flex_column av_four_fifth flex_column_div av-zero-column-padding avia-builder-el-2  el_after_av_one_fifth avia-builder-el-last" style="border-radius:0px;">
                    
                    </div>
				<!--end content-->
				</main>

				<?php

				//get the sidebar
				$avia_config['currently_viewing'] = 'page';
				get_sidebar();

				?>

			</div><!--end container-->

		</div><!-- close default .container_wrap element -->



<?php get_footer(); ?>
