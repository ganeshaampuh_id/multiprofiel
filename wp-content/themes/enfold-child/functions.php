<?php
require_once( 'functions-webwinkel.php');
require_once( 'functions-rekentool.php');
/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/

define( 'CHILD_URL', get_stylesheet_directory_uri() );
define( 'CHILD_IMAGES_URL', CHILD_URL . '/assets/images' );
define( 'CHILD_JS_URL', CHILD_URL . '/assets/js' );
define( 'CHILD_CSS_URL', CHILD_URL . '/assets/css' );

if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Theme Options',
        'menu_title'    => 'Theme Options',
        'menu_slug'     => 'theme-options',
        'capability'    => 'edit_theme_options',
        'redirect'      => false
    ));
    
}

// define the wc_add_to_cart_message callback 
function filter_wc_add_to_cart_message($message, $products, $product_id ) { 
global $woocommerce;
//    $message = '<div class="button wc-forward"><a href="http://www.multiprofiel.nl/webwinkel/" class="button">Continue Shopping</a></div> ' . $message;
    //var_dump($products);
    //return sprintf('<a href="%s" class="button wc-forwards">%s</a> %s', $return_to, __('Continue Shopping', 'woocommerce'), __('%s successfully added to your cart.', 'woocommerce') ); 
    $items = $woocommerce->cart->get_cart();
    $item_datas = '';
    
    $max = sizeof($items);
    $counter = 1;
    foreach($items as $item => $data) {
        if ($counter == $max) {
            $item_datas = $data;
        }
        $counter++;
    }
    
    $productnaam = $item_datas['variation']['attribute_productnaam'];

    $return_to  = get_permalink(woocommerce_get_page_id('shop'));
    $message    = sprintf('%s <div class="wc-forwards" style="display: inline-block;float:right;"><a href="%s" class="button" style="position: relative;">%s</a><a href="http://www.multiprofiel.nl/cart/" class="button" style="position: relative;">Bekijk Winkelwagen</a></div>', __('"'. $productnaam . '" is toegevoegd aan je winkelmand.', 'woocommerce'), $return_to, __('Verder Winkelen', 'woocommerce') );
    return $message;
}; 
         
// add the filter 
add_filter( 'wc_add_to_cart_message', 'filter_wc_add_to_cart_message', 10, 2 ); 