<?php
/*
  Template Name: Systeemvloeren Landing Page
 */
global $avia_config;

add_action( 'wp_head', 'landing_page_scripts' );
function landing_page_scripts(){
	wp_enqueue_script( 'jquery-cycle', CHILD_JS_URL . '/jquery.cycle2.js', array( 'jquery' ), false, false );
	wp_enqueue_script( 'jquery-cycle-optional', CHILD_JS_URL . '/jquery.cycle2.optional.js', array( 'jquery' ), false, false );

	wp_enqueue_style( 'custom-style', CHILD_URL . '/custom.css', false, null, 'all' );
}

/*
 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
 */
get_header();


if (get_post_meta(get_the_ID(), 'header', true) != 'no')
	echo avia_title();

do_action('ava_after_main_title');
?>
<div class='container_wrap container_wrap_first main_color <?php avia_layout_class('main'); ?>'>

	<div class='container'>

		<main class='template-page content <?php avia_layout_class('content'); ?> units' <?php avia_markup_helper( array( 'context' => 'content', 'post_type' => 'page' ) ); ?>>
			<div class="entry-content">
				<h1 id="entresolvloeren-headtitle">Systeemvloeren</h1>

				<div class="flex_column av_one_half flex_column_div first entresolvloeren-box">
					<div class="entresolvloeren-box-wrapper">
						<div class="entresolvloeren-box-container">
							<h3 class="entresolvloeren-title">Wat een systeemvloer <br />
							u <span class="orange">oplevert</span></h3>

							<ul class="entresolvloeren-list">
								<li class="icon-1">Maximaal rendement uit uw bedrijfsruimte</li>
								<li class="icon-2">Eenvoudig en snel uw beschikbare vierkante meters verdubbelen</li>
								<li class="icon-3">Extra opslagruimte, werkplaats, kantoor of showroom</li>
								<li class="icon-4">Eenvoudig uit te breiden en aan te passen</li>
								<li class="icon-5">Flexibel door verplaatsbaarheid</li>
								<li class="icon-6">Binnen 2 weken leverbaar</li>
							</ul>

							<div class="button-wrapper">
								<a class="button button-more" data-tab="tab2" href="<?php echo site_url( 'entresolvloeren' ); ?>">Meer Informatie</a>
							</div>
						</div>
					</div>	
				</div>
				<div class="flex_column av_one_half flex_column_div last entresolvloeren-box">
					<div class="entresolvloeren-box-wrapper entresolvloeren-banner">
						<a href="<?php echo site_url( 'rekentool' ); ?>">
							<img src="<?php echo CHILD_IMAGES_URL; ?>/systeemvloeren-rekentool.jpg" alt="Rekentool Banner" />
						</a>
					</div>
				</div> 
				<!-- end row 1 -->

				<div class="flex_column av_one_half flex_column_div first entresolvloeren-box" id="entresolvloeren-form">
					<h4 class="entresolvloeren-form-title">Vraag direct een offerte aan</h4>
					<div class="entresolvloeren-box-wrapper">
						<div class="entresolvloeren-box-container">
							<?php echo do_shortcode( '[formidable id=15]' ); ?>
						</div>
					</div>
				</div>
				<div class="flex_column av_one_half flex_column_div last entresolvloeren-box" id="entresolvloeren-icon-box">			
					<div class="entresolvloeren-box-wrapper white">
						<h3 class="entresolvloeren-title">Waarom KOPEN bij multiprofiel?</h3>
						<div class="entresolvloeren-box-container white">
							<div class="flex_column av_one_half flex_column_div first entresolvloeren-dot">
								<div class="entresolvloeren-dot-img"><img src="<?php echo CHILD_IMAGES_URL; ?>/dot-1.png" /></div>
								<div class="entresolvloeren-dot-desc">
									Concurrerende prijs door <br />eigen import
								</div>
							</div>
							<div class="flex_column av_one_half flex_column_div half entresolvloeren-dot">
								<div class="entresolvloeren-dot-img"><img src="<?php echo CHILD_IMAGES_URL; ?>/dot-2.png" /></div>
								<div class="entresolvloeren-dot-desc">
									Professioneel montageteam <br />met eigen projectleider
								</div>
							</div>

							<div class="flex_column av_one_half flex_column_div first entresolvloeren-dot">
								<div class="entresolvloeren-dot-img"><img src="<?php echo CHILD_IMAGES_URL; ?>/dot-3.png" /></div>
								<div class="entresolvloeren-dot-desc">
									Binnen 24 uur een <br />passende offerte
								</div>
							</div>
							<div class="flex_column av_one_half flex_column_div half entresolvloeren-dot">
								<div class="entresolvloeren-dot-img"><img src="<?php echo CHILD_IMAGES_URL; ?>/dot-4.png" /></div>
								<div class="entresolvloeren-dot-desc">
									Gecertificeerde kwaliteit en <br />hoog afwerkingsniveau
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>	

				<div class="flex_column av_fullwidth flex_column_div first entresolvloeren-box">
					<div class="entresolvloeren-box-wrapper white">
						<h3 class="entresolvloeren-title">Voorbeelden van onze systeemvloeren</h3>

						<div class="flex_column av_one_fifth flex_column_div first entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/1.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/1.jpg" alt="systeemvloer" border="0" /> </a></div>
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/2.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/2.jpg" alt="verdiepingsvloer" border="0" /> </a></div>
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3Doverzicht1.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3Doverzicht1.jpg" alt="entresolvloer" border="0" /> </a></div>
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3.jpg" alt="tussenvloer" border="0" /> </a></div>
						
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image last"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/4.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/4.jpg" alt="entresolvloer" border="0" /> </a></div>
						

						<div class="flex_column av_one_fifth flex_column_div first entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3Doverzicht2.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3Doverzicht2.jpg" alt="systeemvloer" border="0" /> </a></div>
						
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/6.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/6.jpg" alt="extra etage" border="0" /> </a></div>
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/7.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/7.jpg" alt="entresol" border="0" /> </a></div>
						<div class="flex_column av_one_fifth flex_column_div entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3Doverzicht3.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/3Doverzicht3.jpg" alt="systeemvloer" border="0" /> </a></div>
						<div class="flex_column av_one_fifth flex_column_div last entresolvloeren-image"><a href="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/8.jpg" rel="lightbox[roadtrip]"> <img src="<?php echo CHILD_IMAGES_URL; ?>/entresolvloeren/8.jpg" alt="verdiepingsvloer" border="0" /> </a></div>
						
						
						
						
						
						
						
					</div>
				</div>
				<div class="flex_column av_fullwidth flex_column_div half entresolvloeren-box">
					<div class="entresolvloeren-box-wrapper white">
						<h3 class="entresolvloeren-title">Onze tevreden klanten</h3>

						<?php
							$carouseL_display = 5;
							if( wp_is_mobile() ){
								$carouseL_display = 1;
							}
						?>

					    <div class="carousel-wrapper">
					    	<div class="carousel-container">
						    	<div class="slideshow cycle-slideshow" data-cycle-fx=carousel data-cycle-timeout=5000 data-cycle-carousel-visible=<?php echo $carouseL_display; ?> data-cycle-carousel-fluid=true data-cycle-auto-height=calc data-cycle-slides='> div.slide-item' data-cycle-prev="#carousel-prev" data-cycle-next="#carousel-next">
						    		<div class="slide-item"><div class="slide-item-wrapper"><div class="slide-item-container"><img src="<?php echo CHILD_IMAGES_URL; ?>/landingpage/corepowertools.jpg"></div></div></div>
								    <div class="slide-item"><div class="slide-item-wrapper"><div class="slide-item-container"><img src="<?php echo CHILD_IMAGES_URL; ?>/landingpage/extrabox.jpg"></div></div></div>
								    <div class="slide-item"><div class="slide-item-wrapper"><div class="slide-item-container"><img src="<?php echo CHILD_IMAGES_URL; ?>/landingpage/ibgroep.jpg"></div></div></div>
								    <div class="slide-item"><div class="slide-item-wrapper"><div class="slide-item-container"><img src="<?php echo CHILD_IMAGES_URL; ?>/landingpage/vitakraft.jpg"></div></div></div>
								    <div class="slide-item"><div class="slide-item-wrapper"><div class="slide-item-container"><img src="<?php echo CHILD_IMAGES_URL; ?>/landingpage/volkerstevin.jpg"></div></div></div>
								    <div class="slide-item"><div class="slide-item-wrapper"><div class="slide-item-container"><img src="<?php echo CHILD_IMAGES_URL; ?>/landingpage/wavin.jpg"></div></div></div>
						    	</div>
						    	<a id="carousel-next" href="#" class="carousel-nav"></a>
						    	<a id="carousel-prev" href="#" class="carousel-nav"></a>
						    </div>
						</div>

					</div>
				</div>
			</div><!-- .entry-content -->
		</main>

		<?php
		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();
		?>

	</div><!--end container-->

</div><!-- close default .container_wrap element -->

<!-- Start Of Script Generated By cforms v14.6 [Oliver Seidel | www.deliciousdays.com] -->
<!-- End Of Script Generated By cforms -->
<?php get_footer(); ?>
