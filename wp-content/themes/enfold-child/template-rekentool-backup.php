<?php
/*
  Template Name: Rekentool Backup
 */
global $avia_config;

/*
 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
 */
get_header();


if (get_post_meta(get_the_ID(), 'header', true) != 'no')
	echo avia_title();

do_action('ava_after_main_title');
?>
<?php load_rekentool_scripts();do_shortcode('[formidable id=14]'); ?>
<div class='container_wrap container_wrap_first main_color <?php avia_layout_class('main'); ?>'>

	<div class='container'>

		<main class='template-page content  <?php avia_layout_class('content'); ?> units' <?php avia_markup_helper(array('context' => 'content', 'post_type' => 'page')); ?>>
			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */

			$avia_config['size'] = avia_layout_class('main', false) == 'entry_without_sidebar' ? '' : 'entry_with_sidebar';
			get_template_part('includes/loop', 'page');
			?>

			<div>
				<p class="form-sub" id="rekentool-headtitle"><strong>Bereken direct wat een entresolvloer u zou kosten</strong></p>
				<div id="tabcontainer">
					<form id="rekenform" method="POST" action="/rekentool-offerte">
						<div class="tab" id="tab1">
							<div class="hilite">
								Benieuwd wat een entresolvloer voor uw specifieke magazijn zou kosten? Vul onderstaand formulier in en krijg direct de gemaakte berekening te zien. (De rekentool kan tot en met 500 kg/m² calculeren en houdt rekening met veiligheidsklasse CC1.
Een hoger draagvermogen per m² of een andere veiligheidsklasse op aanvraag. Bij een afwijkende maatvoering kunt een schets sturen naar: <a href="mailto:info@multiprofiel.nl">info@multiprofiel.nl</a>). 
							</div>
							<div style="height: 30px;">
								<ul id="breadcrumb">
									<li>Vraag 1</li>
									<li>&gt; Vraag 2</li>
									<li>&gt; Vraag 3</li>
									<li>&gt; Resultaat</li>
								</ul>
							</div>
							<div class="tabcontent">
								<ol class="numdec">
									<li>Wat wordt de breedte van uw entresolvloer in m</li>

														<input data-bind="value: s1breedte" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="breedte" value="m" /> m <!--<img class="morinfo" src="../wp-content/upload/info_icon.png" alt="" />-->

									<li>Wat wordt de diepte van uw entresolvloer in m</li>

														<input data-bind="value: s1diepte" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="lengte" value="m" /> m <!--<img class="morinfo" src="../wp-content/upload/info_icon.png" alt="" />-->

									<li>Wat wordt de vrije hoogte onderzijde entresolvloer?</li>

									<input data-bind="value: s1hoogte" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="hoogte" value="m" /> m <a href="#" title="Dit is de vrije hoogte, van de grond tot de onderkant van de entresolvloer. Andere klanten kiezen hier gemiddeld voor 3m." class="tooltip"><img src="../wp-content/upload/info_icon.png" alt="" /></a>

									<li>Hoeveel kg dient het draagvermogen van uw entresolvloer te zijn?</li>
									<input data-bind="value: s1draagvermogen" class="numberinput single validate[required,custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" min="0" step="0.01" name="draagvermogen" value="m" /> kg/m<sup>2</sup> <a href="#" title="Wat moet de entresolvloer maximaal kunnen dragen per m2? Andere klanten kiezen hier gemiddeld voor 500kg." class="tooltip"><img src="../wp-content/upload/info_icon.png" alt="" /></a>
								</ol>


							</div>
							<div style="height: 60px;"><a class="formnav next" data-tab="tab2" data-validate="1" href="#stap2">Ga verder</a></div>
						</div>
						<div class="tab" id="tab2">
							<div class="hilite">

								Vul hieronder nog een aantal specifieke vragen in voor een nog nauwkeurige berekening.

							</div>
							<div style="height: 30px;">
								<ul id="breadcrumb">
									<li><a href="#" class="formnav" data-tab="tab1">Vraag 1</a></li>
									<li>&gt; Vraag 2</li>
									<li>&gt; Vraag 3</li>
									<li>&gt; Resultaat</li>
								</ul>
							</div>
							<div>
								<ol class="numdectwo">
									<li>Wenst u een trap?</li>

									<div class="form21" name="form21">
										<input id="jatrap" type="radio" name="s2trap" value="true" data-bind="checked:s2trap.doCheck" />
										<label for="jatrap">ja</label>
										<input id="neetrap" type="radio" name="s2trap" value="false" data-bind="checked:s2trap.doCheck" />
										<label for="neetrap">nee</label>
										<a href="#" title="Sluit u deze entresolvloer aan op een bestaande verdieping waar u reeds een trap heeft, dan is een trap misschien niet nodig." class="tooltip"><img src="../wp-content/upload/info_icon.png" alt="" /></a>
									</div>

									<!--<li>Wilt u luxe houten vloerplaten?</li>

									<div class="form21" name="form22">
										<input id="javloerplaat" type="radio" name="s2vloerplaat" value="false" data-bind="checked:s2vloerplaat.doCheck" />
										<label for="javloerplaat">ja</label>
										<input id="neevloerplaat" type="radio" name="s2vloerplaat" value="true" data-bind="checked:s2vloerplaat.doCheck" />
										<label for="neevloerplaat">nee</label>
										<a href="#" title="Dit zijn de C-DUR de Luxe vloerplaten. Deze zijn afgewerkt met een melamine, anti-slip, grijs gespikkelde toplaag." class="tooltip"><img src="../wp-content/upload/info_icon.png" alt="" /></a>
									</div>-->

									<li>Wenst u een balustrade?</li>

									<div class="form21" name="form23">
										<input id="ja" type="radio" name="s2leuning" value="true" data-bind="checked:s2leuning.doCheck" />
										<label for="ja">ja</label>
										<input id="nee" type="radio" name="s2leuning" value="false" data-bind="checked:s2leuning.doCheck" />
										<label for="nee">nee</label>
										<a href="#" title="Om de veiligheid van uw vloer te waarborgen, raden wij u aan om de open zijden van de entresolvloer te voorzien van leuningwerk." class="tooltip"><img src="../wp-content/upload/info_icon.png" alt="" /></a>
									</div>

									<li data-bind="visible:s2leuning">Indien u een balustrade wenst, hoeveel meter is er benodigd?
										<input data-bind="value: s2leuninglengte" class="numberinput single validate[custom[number]]" onFocus="clearField(this)" onBlur="setField(this)" type="text" step="0.01" name="s2leuninglengte" value="0" /> m <a href="#" title="Graag het totaal aantal strekkende meters invoeren." class="tooltip"><img src="../wp-content/upload/info_icon.png" alt="" /></a>
									</li>
								</ol>
							</div>
							<div style="height: 60px;"><a class="formnav next" data-tab="tab1" href="#stap1">Vorige</a>  <a class="formnav nexttwo" data-tab="tab3" data-validate="2" href="#stap3">Ga verder</a></div>

						</div>
						<div class="tab" id="tab3">
							<div class="hilite"><p>Wilt u de montage door Multi Profiel laten doen?</p></div>
							<div style="height: 30px;">
								<ul id="breadcrumb">
									<li><a href="#" class="formnav" data-tab="tab1">Vraag 1</a></li>
									<li> > <a href="#" class="formnav" data-tab="tab2">Vraag 2</a></li>
									<li> >  Vraag 3</li><li> > Resultaat</li></ul></div>
							<div><ol class="numdecthree">
									<li>Wilt u de montage door Multi Profiel laten doen?</li>
									<div class="form21" name="form31" method="post" action="">
										<input data-bind="checked:s3montage.doCheck" type="radio" name="s3montage" id="ja" value="true" />
										<label for="ja">ja</label>
										<input data-bind="checked:s3montage.doCheck" type="radio" name="s3montage" id="nee" value="false" />
										<label for="nee">nee</label>
										<a href="#" title="Multi Profiel heeft een montageteam die gespecialiseerd is in het monteren van entresolvloeren." class="tooltip"><img src="../wp-content/upload/info_icon.png"  /></a></div>
								</ol>
							</div>
							<div style="height: 60px;">
								<a class="formnav next" data-tab="tab2" href="#stap2">Vorige</a>
								<a class="formnav nexttwo" data-tab="tab4" data-validate="3" href="#stap4">Ga verder</a>

							</div>
						</div>
						<div class="tab" id="tab4">
							<div style="height: 30px;">
								<ul id="breadcrumb">
									<li><a href="#" class="formnav" data-tab="tab1">Vraag 1</a></li>
									<li> > <a href="#" class="formnav" data-tab="tab2">Vraag 2</a></li>
									<li> > <a href="#" class="formnav" data-tab="tab3">Vraag 3</a></li>
									<li> > Resultaat</li></ul>
							</div>
							<!--<div><p style="border-radius:5px; padding:8px; vertical-align:middle; font-size:16px !important; font-weight:bold; width:30px; height:25px; text-align:center; background-color:#ff931d; color:#fff;">1/2</p><ul class="resultaat">
										<li>Totaalprijs materiaal entresolvloer </li><span style="margin-left: 215px;position: absolute;margin-top: -30px;">&euro;</span><span class="resultinput"><input data-bind="value: prijsvloerkortingdisplay" type="text" name="prijsmateriaal" readonly  class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)"></span>
										<li>Totaalprijs accessoires </li><span style="margin-left: 215px;position: absolute;margin-top: -30px;">&euro;</span><span class="resultinput"><input data-bind="value: prijsaccessoiresdisplay" readonly  type="text" name="prijsaccessoires"  class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)"></span>
										<li>Montage kosten </li><span style="margin-left: 215px;position: absolute;margin-top: -30px;">&euro;</span><span class="resultinput"><input data-bind="value: prijsmontagedisplay" type="text" name="prijsmontage" readonly class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)"></span>

										<li style="border-top: 2px solid #D8D8D8;">Totale kosten </li><span style="margin-left: 215px;position: absolute;margin-top: -30px;">&euro;</span><span class="resultinput"><input data-bind="value: prijstotaal" readonly  type="text" name="prijstotaal" class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)"></span>
									</ul>
							</div><br>-->
							<input data-bind="value: prijsvloerkortingdisplay" type="hidden" name="prijsmateriaal" readonly  class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
							<input data-bind="value: prijsaccessoiresdisplay" readonly  type="hidden" name="prijsaccessoires"  class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
							<input data-bind="value: prijsmontagedisplay" type="hidden" name="prijsmontage" readonly class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
							<input data-bind="value: prijstotaal" readonly  type="hidden" name="prijstotaal" class="singletwo" value="euro" onFocus="clearField(this)" onBlur="setField(this)">
							<div class="hilite">
								<p>Vul hieronder uw gegevens in, u ontvangt direct een prijs ter indicatie per e-mail! Wij nemen z.s.m. contact met u op om de antwoorden te bespreken. Vervolgens ontvangt u een vrijblijvende offerte van ons, mocht dit wenselijk zijn!</p>
							</div>
							<div>
							<!--<p style="border-radius:5px; padding:8px; vertical-align:middle; font-size:16px !important; font-weight:bold; width:30px; height:25px; text-align:center; background-color:#ff931d; color:#fff;">2/2</p>--><ul class="contactperson">
									<li>Naam Contactpersoon </li><span class="resultinput"><input type="text" name="naam"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>Naam Bedrijf </li><span class="resultinput"><input type="text" name="naambedrijf"  class="single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>Land </li><span class="resultinput"><input type="text" name="land"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>Plaats </li><span class="resultinput"><input type="text" name="plaats"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>Adres </li><span class="resultinput"><input type="text" name="adres"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>Postcode </li><span class="resultinput"><input type="text" name="postcode"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>Telefoonnummer </li><span class="resultinput"><input type="text" name="telefoon"  class="validate[required] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
									<li>E-mail adres </li><span class="resultinput"><input type="text" name="email"  class="validate[required,custom[email]] single" value="" onFocus="clearField(this)" onBlur="setField(this)"></span>
								</ul>
							</div>
							<div style="height: 60px;"><!--<span class="verder"><a class="formnav" data-tab="tab3" href="#stap3">Vorige</a></span>--><input type="submit" style="border:none; padding:9px; cursor:pointer" value="Berekening Insturen" class="verderthree"></span>
							</div>
						</div>
					</form>
				</div>
				<script>
					jQuery = jQuery.noConflict();
					jQuery(document).ready(function () {

						jQuery('input[type="text"]').click(function () {
							if (jQuery(this).val() == '0') {
								jQuery(this).val('');
							}
						});

						jQuery('#rekenform').validationEngine();

						jQuery('#tabcontainer .tab').hide();
						jQuery('#tabcontainer #tab1').show();

						//jQuery('#tab4 .formnav.nexttwo').click(function(){
						//	jQuery.ajax({
						//		type: "POST",
						//		url: "offerte.php?stap=4",
						//		data: jQuery("#rekenform").serialize()
						//	}).done(function() {
						//
						//	});
						//});

						jQuery('.formnav').click(function () {
							var tab = jQuery(this).data('tab');
							var valtab = jQuery(this).data('validate');

							//console.log(tab);
							//console.log(valtab);

							var isvalid = true;



							if (jQuery(this).hasClass('next') || jQuery(this).hasClass('nexttwo')) {
								jQuery('#tab' + valtab + ' input.required').each(function () {
									//console.log(jQuery(this));
									if (jQuery(this).val() == 0) {
										isvalid = false;
										jQuery(this).css('border', '1px solid #c00');
									} else {
										jQuery(this).css('border', '1px solid #bfbfbf');
									}
								})
								if (isvalid == true) {
									jQuery('#tabcontainer .tab').hide();
									jQuery('#tabcontainer #' + tab).show();
								}
							} else {
								jQuery('#tabcontainer .tab').hide();
								jQuery('#tabcontainer #' + tab).show();
							}

						});

						// jQuery("input[type=number]").on("keydown", function(e) {
                        //     console.log(e.keyCode);
                        //     if(e.keyCode === 190 || e.keyCode === 110) {
                        //         this.value.replace('.', ',');
                        //         e.preventDefault();
                        //     }
                        // });
					});
					jQuery('#rekenform').keypress(function (event) {
						if (event.keyCode == 10 || event.keyCode == 13)
							event.preventDefault();
					});

                    jQuery.fn.ForceNumericOnly =
                    function()
                    {
                		return this.each(function()
                		{
                			jQuery(this).keydown(function(e)
                			{
                                if(e.keyCode == 188){
                                    e.preventDefault();
                                    jQuery(this).val(jQuery(this).val() + '.');
                                }
                				var key = e.charCode || e.keyCode || 0;
                				return (
                					key == 8 ||
                					key == 9 ||
                					key == 46 ||
                					key == 110 ||
                                    key == 188 ||
                					key == 190 ||
                					(key >= 35 && key <= 40) ||
                					(key >= 48 && key <= 57) ||
                					(key >= 96 && key <= 105));

                			});
                		});
                	};
                    jQuery(".numberinput").ForceNumericOnly();
				</script>

				<script src="<?php echo site_url(); ?>/rekentool-assets/knockout.js"></script>
				<script src="<?php echo site_url(); ?>/rekentool-assets/calc.js"></script>
				<!--<hr />

				<div class="images_top">
					<div class="images_top-row first"><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-02.jpg" rel="lightbox[roadtrip]"><img class="first" style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-2-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-03.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-3-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-04.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-4-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-05.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-5-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a></div>
					<div class="images_top-row first"><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-06.jpg" rel="lightbox[roadtrip]"><img class="first" style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-6-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-07.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-7-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-08.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-8-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a><a href="http://www.multiprofiel.nl/wp-content/upload/Entresolvloeren-01.jpg" rel="lightbox[roadtrip]"><img style="border: 0px;" title="Entresolvloeren - Multiprofiel" src="http://www.multiprofiel.nl/wp-content/upload/entresolvloeren-9-150x150.jpg" alt="entresolvloeren" width="120" height="80" border="0" /></a></div>
					</div>-->
			</div><!-- .entry-content -->
		</main>

		<?php
		//get the sidebar
		$avia_config['currently_viewing'] = 'page';
		get_sidebar();
		?>

	</div><!--end container-->

</div><!-- close default .container_wrap element -->

<!-- Start Of Script Generated By cforms v14.6 [Oliver Seidel | www.deliciousdays.com] -->
<!-- End Of Script Generated By cforms -->

<!-- Google Code for Lijst 15 mei 2013 -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1070683703;
var google_conversion_label = "9LtXCJmmvgUQt6zF_gM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="<?php echo site_url(); ?>/rekentool-assets/conversion.js">
</script>
<?php get_footer(); ?>
