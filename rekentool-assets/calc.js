// amount, delimiter, thousandseparator
Number.prototype.formatMoney = function(c, d, t){
	var n = this,
	c = isNaN(c = Math.abs(c)) ? 2 : c,
	d = d == undefined ? "," : d,
	t = t == undefined ? "." : t,
	s = n < 0 ? "-" : "",
	i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
	j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function MPModel() {
	this.transport	= ko.observable(350);
	this.schoorkolommen = ko.observable(225);
	this.kruisschoor = ko.observable(125);
	this.palletopzetplaat = ko.observable(300);

	this.s1breedte = ko.observable(0);
	this.s1diepte = ko.observable(0);
	this.s1hoogte = ko.observable(0);
	this.s1draagvermogen = ko.observable(0);

	this.s2trap = ko.observable(true);
	this.s2vloerplaat = ko.observable(false);
	this.s2leuning = ko.observable(true);
	this.s2leuninglengte = ko.observable(0);

	this.s3montage = ko.observable(true);

	this.s2trap.doCheck = ko.computed({
		read: function() {
			return this.s2trap().toString();
		},
		write: function(newValue) {
			this.s2trap(newValue === "true");
		},
		owner: this
	});
	this.s2vloerplaat.doCheck = ko.computed({
		read: function() {
			return this.s2vloerplaat().toString();
		},
		write: function(newValue) {
			this.s2vloerplaat(newValue === "true");
		},
		owner: this
	});
	this.s2leuning.doCheck = ko.computed({
		read: function() {
			return this.s2leuning().toString();
		},
		write: function(newValue) {
			this.s2leuning(newValue === "true");
		},
		owner: this
	});
	this.s3montage.doCheck = ko.computed({
		read: function() {
			return this.s3montage().toString();
		},
		write: function(newValue) {
			this.s3montage(newValue === "true");
		},
		owner: this
	});

	// kosten vloer
	this.m2 = ko.computed(function(){
		return (this.s1breedte()) * (this.s1diepte());
	},this);
	this.kolommen = ko.computed(function(){
		return this.m2() * 16;
	},this);
	this.hoofdbalken = ko.computed(function(){
		return this.m2() * 8;
	},this);
	this.kinderbalken = ko.computed(function(){
		return this.m2() * 14;
	},this);
	this.hoeken = ko.computed(function(){
		return this.m2() * 2;
	},this);
	this.cdur = ko.computed(function(){
		return this.m2() * 17;
	},this);
	this.kleinmateriaal = ko.computed(function(){
		return this.m2() * 1.5;
	},this);
	this.prijsvloer = ko.computed(function(){
		return (this.kolommen() + this.hoofdbalken() + this.kinderbalken() + this.hoeken() + this.cdur() + this.kleinmateriaal() + 1050.5);
	},this);

	// kosten accessoires
	this.grico = ko.computed(function(){
		if(this.s2vloerplaat() == true){
			return (this.m2() * 6.16);
		}
		else{
			return false;
		}
	},this);
	this.trappen = ko.computed(function(){
		if(this.s2trap() == true){
			return (1050);
		}
		else{
			return false;
		}
	},this);
	this.leuning = ko.computed(function(){
		if(this.s2leuning() == true){
			return (this.s2leuninglengte() * 37.5);
		}
		else{
			return false;
		}
	},this);
	this.prijsaccessoires = ko.computed(function(){
		return (this.grico() + this.trappen() + this.leuning());
	},this);

	// montage kosten
	this.prijsmontage = ko.computed(function(){
		if(this.s3montage() == true && (this.m2() * 2) <= 50){ 
			return (600);
		}
		if(this.s3montage() == true && (this.m2() * 2) > 50){
			return (this.m2()*12);
		}
		if(this.s3montage() == false){
			return (0);
		}
	},this);

	this.prijsmontagecalc = ko.computed(function(){
		return Math.ceil(this.prijsmontage() / 325) * 325;
	},this);
	this.prijsvloerdisplay = ko.computed(function(){
		return (this.prijsvloer()).formatMoney();
	},this);
	this.prijsaccessoiresdisplay = ko.computed(function(){
		return (this.prijsaccessoires()).formatMoney();
	},this);
	this.prijsmontagedisplay = ko.computed(function(){
		return (this.prijsmontagecalc()).formatMoney();
	},this);
	this.prijstransport = ko.computed(function(){
		return (this.transport()).formatMoney();
	},this);
	this.prijskruisschoor = ko.computed(function(){
		return (this.kruisschoor()).formatMoney();
	},this);
	this.prijsschoorkolommen = ko.computed(function(){
		return (this.schoorkolommen()).formatMoney();
	},this);
	this.prijspalletopzetplaat = ko.computed(function(){
		return (this.palletopzetplaat()).formatMoney();
	},this);



	this.prijsvloerkorting = ko.computed(function(){
		var totaalprijs;
		// 25% korting tot 5000

		if (this.prijsvloer() <= 5000){
			totaalprijs = (this.prijsvloer() - ( this.prijsvloer() * .07 ));
			//console.log('prijs: '+this.prijsvloer()+' & korting 25%: '+totaalprijs);
			return totaalprijs;
		}
		// 30% korting tussen 5000 & 10000
		if (this.prijsvloer() > 5000 && this.prijsvloer() <= 10000){
			totaalprijs = (this.prijsvloer() - ( this.prijsvloer() * .1 ));
			//console.log('prijs: '+this.prijsvloer()+' & korting 30%: '+totaalprijs);
			return totaalprijs;
		}
		// 35% korting boven 10000
		if (this.prijsvloer() > 10000){
			totaalprijs = (this.prijsvloer() - ( this.prijsvloer() * .15 ));
			//console.log('prijs: '+this.prijsvloer()+' & korting 35%: '+totaalprijs);
			return totaalprijs;
		}
		// anders geen korting
		else{
			totaalprijs = (this.prijsvloer());
			//console.log('geen korting: '+totaalprijs);
			return totaalprijs;
		}

	},this);

	this.prijsvloerkortingdisplay = ko.computed(function(){
		return this.prijsvloerkorting().formatMoney();
	},this)

	this.prijstotaal = ko.computed(function(){
		return (parseFloat(this.prijsvloerkorting()) + parseFloat(this.prijsaccessoires()) + parseFloat(this.prijsmontagecalc())).formatMoney(2);
	},this);
};
ko.applyBindings(new MPModel);
