jQuery(document).ready(function() {
	jQuery("a#nav_offerte, a.btn_offerte").fancybox({
		autoDimensions: false,
		autoScale: false,
		width: 460,
		height: 580,
		href: '#offerte_form'
	});

	jQuery('a[rel="fancybox"]').fancybox();
	
	jQuery('.hentry .entry-content').each(function(i){
		var art_img = jQuery(this).parent('.article-content').prev('.article-img');
		jQuery(this).find('img').parent('a,p').appendTo(art_img);
	});
	
	jQuery('#smooth_slider .jcarousel-control a').click(function(){
		jQuery('#smooth_slider .jcarousel-control a').removeClass('active');
		jQuery(this).addClass('active');
	});
});


function setSliderNav(carousel) 
{
	jQuery('#smooth_slider .jcarousel-item').each(function(i){
		var title = jQuery(this).find('h2').text();

		jQuery('#smooth_slider .jcarousel-control a:nth('+i+')').text(title);
	});
}