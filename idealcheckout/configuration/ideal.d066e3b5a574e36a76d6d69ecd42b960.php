<?php

	/*
		This plug-in was developed by iDEAL Checkout.
		See www.ideal-checkout.nl for more information.

		This file was generated on 06-01-2017, 07:46:05
	*/


	// Basic gateway settings
	$aSettings['GATEWAY_NAME'] = 'iDEAL Simulator - iDEAL';
	$aSettings['GATEWAY_WEBSITE'] = 'http://www.ideal-simulator.nl/';
	$aSettings['GATEWAY_METHOD'] = 'ideal-simulator';
	$aSettings['GATEWAY_VALIDATION'] = false;


	// E-mailadresses for transaction updates (comma seperated)
	$aSettings['TRANSACTION_UPDATE_EMAILS'] = 'nebliena@gmail.com';

?>