<?php

	/*
		This plug-in was developed by iDEAL Checkout.
		See www.ideal-checkout.nl for more information.

		This file was generated on 10-01-2017, 09:33:28
	*/


	// Webshop-ID
	$aSettings['MERCHANT_ID'] = '220107601900001';

	// Your iDEAL Sub ID
	$aSettings['SUB_ID'] = '0';

	// Password used to generate hash
	$aSettings['HASH_KEY'] = 'p5pE_L8ziYWYGKk8DI1dv0XTt8vBnDScakCuWj4WgHQ';
	$aSettings['KEY_VERSION'] = '2';

	// Use TEST/LIVE mode; true=TEST, false=LIVE
	$aSettings['TEST_MODE'] = false;


	// Basic gateway settings
	$aSettings['GATEWAY_NAME'] = 'Rabo OmniKassa - Visa';
	$aSettings['GATEWAY_WEBSITE'] = 'http://www.rabobank.nl/';
	$aSettings['GATEWAY_METHOD'] = 'visa-omnikassa';
	$aSettings['GATEWAY_VALIDATION'] = false;


	// E-mailadresses for transaction updates (comma seperated)
	$aSettings['TRANSACTION_UPDATE_EMAILS'] = 'barbara@u-digital.nl, sam@u-digital.nl';

?>