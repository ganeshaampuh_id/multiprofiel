<?php

	/*
		Let us help you to create a suitable configuration file for your iDEAL Checkout plug-in.
		Go to: http://www.ideal-checkout.nl/
	*/


	// Webshop-ID
	$aSettings['MERCHANT_ID'] = '002020000000001';

	// Your iDEAL Sub ID
	$aSettings['SUB_ID'] = '0';

	// Password used to generate hash
	$aSettings['HASH_KEY'] = '002020000000001_KEY1';
	$aSettings['KEY_VERSION'] = '1';

	// Use TEST/LIVE mode; true=TEST, false=LIVE
	$aSettings['TEST_MODE'] = true;


	// Basic gateway settings
	$aSettings['GATEWAY_NAME'] = 'Rabo OmniKassa - iDEAL';
	$aSettings['GATEWAY_WEBSITE'] = 'http://www.rabobank.nl/';
	$aSettings['GATEWAY_METHOD'] = 'vpay-omnikassa';
	$aSettings['GATEWAY_VALIDATION'] = false;


?>